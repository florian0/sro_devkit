#include "TypeId.h"

namespace TypeIdRegistry {
    TID_2<3, 2> ITEM_COS;
    TID_2<3, 3> ITEM_ETC;
    TID_3<3, 3, 4> ITEM_ETC_AMMO;
    TID<3, 3, 3, 5> ITEM_ETC_SCROLL_GLOBALCHATTING;
}
