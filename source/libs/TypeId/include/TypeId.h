#pragma once

#include "TypeIdBase.h"

namespace TypeIdRegistry {
    extern TID_2<3, 2> ITEM_COS;
    extern TID_2<3, 3> ITEM_ETC;
    extern TID_3<3, 3, 4> ITEM_ETC_AMMO;
    extern TID<3, 3, 3, 5> ITEM_ETC_SCROLL_GLOBALCHATTING;
}
