//-----------------------------------------------------
// This file contains structs, defines, enums, etc..
// that's getting use in both client and server
//-----------------------------------------------------

#pragma once

#include <Windows.h>

//===========================================================
#define SR_SPECIAL_COMMAND_RECALL_PLAYER (WORD) 1
#define SR_SPECIAL_COMMAND_MOVE_TO_TOWN (WORD) 2
#define SR_SPECIAL_COMMAND_MOVE_TARGET_TO_TOWN (WORD) 3
#define SR_SPECIAL_COMMAND_SHOW_ME_STATICS (WORD) 4
#define SR_SPECIAL_COMMAND_SHOW_ME_CHAR_DATA (WORD) 5
#define SR_SPECIAL_COMMAND_SUMMON_MONSTER (WORD) 6
#define SR_SPECIAL_COMMAND_DROP_ITEM (WORD) 7
#define SR_SPECIAL_COMMAND_MOVE_TO_PLAYER (WORD) 8
// 9
#define SR_SPECIAL_COMMAND_SET_GAME_TIME (WORD) 10
// 11
#define SR_SPECIAL_COMMAND_FORCE_KILL_MOB (WORD) 12
#define SR_SPECIAL_COMMAND_BAN_PLAYER (WORD) 13
#define SR_SPECIAL_COMMAND_INVISIBLE (WORD) 14
#define SR_SPECIAL_COMMAND_INVINCIBLE (WORD) 15
#define SR_SPECIAL_COMMAND_MOVE_TO_WP (WORD) 16
// There is alot but im lazy, sorry sorry :P .....
//===========================================================