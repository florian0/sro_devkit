#include "IFScrollBar.h"

void CIFScrollBar::FUN_006590e0(short a2) {
    /// \todo Implement this function
    reinterpret_cast<void(__thiscall *)(CIFScrollBar *, short)>(0x006590e0)(this, a2);
}

void CIFScrollBar::FUN_00659150(int a2) {
    /// \todo Implement this function
    reinterpret_cast<void(__thiscall *)(CIFScrollBar *, int)>(0x00659150)(this, a2);
}

void CIFScrollBar::FUN_006592a0(int a2, int a3, int a4) {
    /// \todo Implement this function
    reinterpret_cast<void(__thiscall *)(CIFScrollBar *, int, int, int)>(0x006592a0)(this, a2, a3, a4);
}
