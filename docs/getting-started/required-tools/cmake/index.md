# CMake

Download *Windows x64 Installer" from here: https://cmake.org/download/

## Installation

Run the installer

![cmake-1](./cmake-1.png)

Read carefully through the agreement to avoid purchasing any unwanted cloth 
washers.

![cmake-2](cmake-2.png)

Choose: Add CMake to PATH for **ALL USERS**. If you do not, you will have a 
lot of pain.

![cmake-3](cmake-3.png)

Choose the installation folder. Usually doesn't require to be changed.

![cmake-4](cmake-4.png)

Ready to install? Let's go!

![cmake-5](cmake-5.png)

... wait until it's done.

![cmake-6](cmake-6.png)

Woohoo. You successfully completed the quest: *Install CMake*.

![cmake-7](cmake-7.png)
