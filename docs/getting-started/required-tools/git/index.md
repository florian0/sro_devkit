# Git

Download [64-bit Git for Windows Setup](https://git-scm.com/download/win)

Follow these steps:

1. Read the EULA.  
   ![Read the EULA](1.png)

2. Keep the folder default.  
   ![Alt text](2.png)

3. Keep everything default.  
   ![Alt text](3.png)
   ![Alt text](4.png)

4. Pick Visual Studio Code from the list.  
   ![Alt text](5.png)

5. Pick the bottom entry.  
   ![Alt text](6.png)

6. Keep everything default.  
   ![Alt text](7.png)
   ![Alt text](8.png)
   ![Alt text](9.png)
   ![Alt text](10.png)
   ![Alt text](11.png)
   ![Alt text](12.png)
   ![Alt text](13.png)
   ![Alt text](14.png)
   ![Alt text](15.png)

7. Wait until its installed.  
   ![Alt text](16.png)

8. Untick all boxes. You're done.  
   ![Alt text](17.png)
